package com.portfoliobackend.portfoliobackendjava.controller;

import com.portfoliobackend.portfoliobackendjava.entity.Aws;
import com.portfoliobackend.portfoliobackendjava.entity.Backend;
import com.portfoliobackend.portfoliobackendjava.entity.DatabaseTable;
import com.portfoliobackend.portfoliobackendjava.entity.Frontend;
import com.portfoliobackend.portfoliobackendjava.service.AwsService;
import com.portfoliobackend.portfoliobackendjava.service.BackendService;
import com.portfoliobackend.portfoliobackendjava.service.DatabaseService;
import com.portfoliobackend.portfoliobackendjava.service.FrontEndService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PortfolioController {

    @Autowired
private AwsService awsService;

    @Autowired
private BackendService backendService;

    @Autowired
    private FrontEndService frontEndService;

    @Autowired
    private DatabaseService databaseService;

@GetMapping("/awsData")
public List<Aws> findAllAwsData(){
    return awsService.getAws();
}

@GetMapping("/backendData")
public List<Backend> getBackendData(){
    return backendService.getBackendData();
}

@GetMapping("/frontendData")
    public List<Frontend> getFrontendData(){
    return frontEndService.getFrontendData();
}

@GetMapping("/databaseData")
    public List<DatabaseTable> getDatabaseData(){
    return databaseService.getDatabaseDetails();
}
}
