package com.portfoliobackend.portfoliobackendjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortfoliobackendjavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PortfoliobackendjavaApplication.class, args);
    }

}
