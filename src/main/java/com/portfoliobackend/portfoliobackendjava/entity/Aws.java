package com.portfoliobackend.portfoliobackendjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Aws {
    @Id
    @GeneratedValue
    private int id;
    private String caption;
    private String link;

}
