package com.portfoliobackend.portfoliobackendjava.repository;

import com.portfoliobackend.portfoliobackendjava.entity.Backend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BackendInterface extends JpaRepository<Backend,Integer> {
}
