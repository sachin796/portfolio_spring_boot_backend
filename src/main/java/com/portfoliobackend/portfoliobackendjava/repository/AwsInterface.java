package com.portfoliobackend.portfoliobackendjava.repository;

import com.portfoliobackend.portfoliobackendjava.entity.Aws;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AwsInterface extends JpaRepository<Aws,Integer> {

}
