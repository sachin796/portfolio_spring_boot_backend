package com.portfoliobackend.portfoliobackendjava.repository;

import com.portfoliobackend.portfoliobackendjava.entity.Frontend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FrontendInterface extends JpaRepository<Frontend,Integer> {
}
