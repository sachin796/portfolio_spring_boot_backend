package com.portfoliobackend.portfoliobackendjava.repository;

import com.portfoliobackend.portfoliobackendjava.entity.DatabaseTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DatabaseInterface extends JpaRepository<DatabaseTable,Integer> {
}
