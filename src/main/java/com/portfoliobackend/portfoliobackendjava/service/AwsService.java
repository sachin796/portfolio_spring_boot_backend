package com.portfoliobackend.portfoliobackendjava.service;

import com.portfoliobackend.portfoliobackendjava.entity.Aws;
import com.portfoliobackend.portfoliobackendjava.repository.AwsInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AwsService {

    @Autowired
    private AwsInterface awsInterface;

    public List<Aws> getAws(){
return awsInterface.findAll();
    }

}
