package com.portfoliobackend.portfoliobackendjava.service;

import com.portfoliobackend.portfoliobackendjava.entity.Backend;
import com.portfoliobackend.portfoliobackendjava.repository.BackendInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BackendService {

    @Autowired
    private BackendInterface backendInterface;

    public List<Backend> getBackendData(){
        return backendInterface.findAll();
    }


}
