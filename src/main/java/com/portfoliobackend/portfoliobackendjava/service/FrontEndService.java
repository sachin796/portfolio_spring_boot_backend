package com.portfoliobackend.portfoliobackendjava.service;

import com.portfoliobackend.portfoliobackendjava.entity.Frontend;
import com.portfoliobackend.portfoliobackendjava.repository.FrontendInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FrontEndService {

    @Autowired
    private FrontendInterface frontendInterface;

    public List<Frontend> getFrontendData(){
        return frontendInterface.findAll();
    }

}
