package com.portfoliobackend.portfoliobackendjava.service;

import com.portfoliobackend.portfoliobackendjava.entity.DatabaseTable;
import com.portfoliobackend.portfoliobackendjava.repository.DatabaseInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DatabaseService {

    @Autowired
    private DatabaseInterface databaseInterface;

    public List<DatabaseTable> getDatabaseDetails(){
        return databaseInterface.findAll();
    }

}
